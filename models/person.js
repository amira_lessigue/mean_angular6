var mongoose = require('mongoose');

var PersonSchema = new mongoose.Schema({
  name: String,
  Date_of_birth: Date,
  Document_ID: String ,
  Sex : String,
  Address: String,
 
});
module.exports = mongoose.model('Person', PersonSchema);