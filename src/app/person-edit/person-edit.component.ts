import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-person-edit',
  templateUrl: './person-edit.component.html',
  styleUrls: ['./person-edit.component.css']
})
export class PersonEditComponent implements OnInit {
  personForm: FormGroup;
  id:string = '';
  Name:string = '';
  Date_of_birth:Date = '';
  Document_ID:string = '';
  Sex:string = '';
  Address:string = '';
  
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
  this.getBook(this.route.snapshot.params['id']);
  this.bookForm = this.formBuilder.group({
    'Name' : [null, Validators.required],
    'Date_of_birth' : [null, Validators.required],
    'Document_ID' : [null, Validators.required],
    'Sex' : [null, Validators.required],
    'Address' : [null,],
    
  });
  }

};

getPerson(id) {
  this.api.getPerson(id).subscribe(data => {
    this.id = data._id;
    this.personForm.setValue({
      Name: data.Name,
      Date_of_birth: data.Date_of_birth,
      Document_ID: data.Document_ID,
      Sex: data.Sex,
      Address: data.Address,
         });
  });
}
