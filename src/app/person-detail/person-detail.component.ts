import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.css']
})
export class PersonDetailComponent implements OnInit {
  person = {};
  constructor(private route: ActivatedRoute, private api: ApiService) { }

  ngOnInit() {
  }

};

getPersonDetails(id) {
  this.api.getPerson(id)
    .subscribe(data => {
      console.log(data);
      this.person = data;
    });
}
