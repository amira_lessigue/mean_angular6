import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';

displayedColumns = ['Name', 'Date_of_birth', 'Document_ID','Sex','Address'];
dataSource = new BookDataSource(this.api);

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {
  persons: any; 
  constructor(private api: ApiService) { }

  ngOnInit() {
   this.api.getPersons()
    .subscribe(res => {
      console.log(res);
      this.persons = res;
    }, err => {
      console.log(err);
    });
  }

}

export class PersonDataSource extends DataSource<any> {
  constructor(private api: ApiService) {
    super()
  }

  connect() {
    return this.api.getPersons();
  }

  disconnect() {

  }
}
